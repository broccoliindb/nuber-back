import { Resolver, Query, Args, Mutation } from '@nestjs/graphql'
import { Restaurant } from './entities/restaurant.entity'
import { CreateRestaurantDto } from './dtos/crate-restaurant.dto'

@Resolver(() => Restaurant)
export class RestaurantsResolver {
  @Query(() => [Restaurant])
  restaurants(@Args('veganOnly') veganOnly: boolean): Restaurant[] {
    console.log(veganOnly)
    return []
  }
  @Mutation(() => Boolean)
  craterestaurant(@Args() createRestaurantInput: CreateRestaurantDto): boolean {
    console.log(createRestaurantInput)
    return true
  }
}
